#include "iap_pro.h"
#include "stm32f10x_conf.h"


int main()
{
    UpdateRunFlag flg;
    FLASH_Unlock();
    GetRunFlag(&flg);
    //升级标志位正常，则说明APP2里面没有要升级的程序，直接跳转到APP1
    if(flg.UpdateHandle == 0xFFFFFFFF || flg.UpdateHandle == APP_RUN_FLG)
    {
        if(App1Vaild())
        {
            JumpApp1();
        }
    }
    else
    {
        //说明APP2里面有要升级的内容，要判断是否合法
        if(App2Vaild())
        {
            CopyToApp1();   //把APP2内容拷贝到APP1
            if(App1Vaild()) //判断是否合法
            {
                JumpApp1(); //调整到APP1
            }
        }
    }
    while(1);
}
