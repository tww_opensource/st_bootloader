#ifndef _IAP_PRO_H_
#define _IAP_PRO_H_
#include <stdint.h>

//APP1 是主要运行程序，APP2是备份程序

#define     APP_RUN_FLG         0x11223344
#define     UPDATE_FLG          0x55443322

//升级运行标记
typedef struct
{
    uint32_t     UpdateHandle;       //升级操作
    uint32_t     App2Size;           //APP2的长度
    uint8_t      App2SHA256[32];           //APP2的SHA256
    uint32_t     App1Size;          //APP1长度
    uint8_t      App1SHA256[32];    
}UpdateRunFlag;


#define FLASH_PAGE_SIZE                     2048

//BOOT程序起始地址
#define FLASH_BOOT_ADDRESS                  0x8000000

//升级运行标记地址
#define FLASH_UPDATE_FLAG_ADDRESS           0x8007800

//Bootloader版本地址
#define FLASH_BOOTLOADER_VER_ADDRESS        0x8001000

//定义APP程序最大占用空间(这个大小根据具体芯片来)
#define APP_MAX_SIZE                        0x1C000

//APP1程序地址
#define FLASH_APP1_ADDRESS                  0x8008000

//APP2程序地址
#define FLASH_APP2_ADDRESS                  (FLASH_APP1_ADDRESS + APP_MAX_SIZE)

//内部版本号偏移地址
#define FLASH_VERSION_OFFSET                0x1000

/**
* @函数名:      FlashProgramStart
* @功能描述:    解锁FLASH
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void FlashProgramStart(void);

/**
* @函数名:      ProgramFlashWord
* @功能描述:    把data数据烧写到address，烧写长度为len字节，len必须4字节对齐，否则烧写异常
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void ProgramFlash(uint32_t address,uint8_t* data,uint32_t len);


/**
* @函数名:      EraseApp1
* @功能描述:    擦除APP1
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void EraseApp1(void);

/**
* @函数名:      EraseApp2
* @功能描述:    擦除APP2
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void EraseApp2(void);

/**
* @函数名:      CopyToApp2
* @功能描述:    把APP1里面的内容拷贝到APP2
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void CopyToApp2(void);

/**
* @函数名:      CopyToApp1
* @功能描述:    把APP2里面的内容拷贝到APP1
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void CopyToApp1(void);

/**
* @函数名:      App1Vaild
* @功能描述:    App1程序是否有效
* @输入参数:    无
* @输出参数:    无
* @返回值:      0:无效,其他:有效
*/
int App1Vaild(void);

/**
* @函数名:      App2Vaild
* @功能描述:    App2程序是否有效
* @输入参数:    无
* @输出参数:    无
* @返回值:      0:无效,其他:有效
*/
int App2Vaild(void);

/**
* @函数名:      GetRunFlag
* @功能描述:    获取运行标志
* @输入参数:    运行标志
* @输出参数:    无
* @返回值:      无
*/
void GetRunFlag(UpdateRunFlag* flg);

/**
* @函数名:      SetRunFlag
* @功能描述:    设置运行标志
* @输入参数:    运行标志
* @输出参数:    无
* @返回值:      无
*/
void SetRunFlag(UpdateRunFlag* flg);

/**
* @函数名:      SetUpdateRunFlag
* @功能描述:    设置运行标志位
* @输入参数:    运行标志位
* @输出参数:    无
* @返回值:      无
*/
void SetUpdateRunFlag(uint32_t flg);

/**
* @函数名:      JumpApp1
* @功能描述:    跳转到APP1
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void JumpApp1(void);

/**
* @函数名:      Reboot
* @功能描述:    重启
* @输入参数:    无
* @输出参数:    无
* @返回值:      无
*/
void Reboot(void);

#endif
